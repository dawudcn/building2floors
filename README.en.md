# 建筑轮廓转分层分户模型

#### Description
在三维 Web GIS 可视化解决方案中，矢量建筑模型无疑是可大幅提升可视化效果的基础数据。
传统的使用场景是智慧城市电子沙盘，而面对更加微观的场景，例如小区、楼宇、园区的管理，我们需要每层每户的信息，因而产生了由建筑轮廓转分层分户的需求。本仓库目的就是用批量灵活的方法去处理原始数据得到楼层模型数据。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)